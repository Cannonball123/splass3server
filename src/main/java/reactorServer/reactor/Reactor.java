package reactorServer.reactor;

import messages.FixedSeparatorMessageTokenizer;
import messages.MessageTokenizer;
import messages.TokenizerFactory;
import protocol.AsyncServerProtocol;
import protocol.AsyncServerProtocolFactory;
import reactorServer.protocol.TBGP;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;



/**
 * An implementation of the Reactor pattern.
 */
public class Reactor implements Runnable {

    private static final Logger logger = Logger.getLogger("edu.spl.reactor");

    private final int port;
    private final int poolSize;
    private final AsyncServerProtocolFactory asyncServerProtocolFactory;
    private final TokenizerFactory tokenizerFactory;

    private volatile boolean shouldRun = true;
    private ReactorData reactorData;

    /**
     * Creates a new Reactor
     *
     * @param port      					the port to bind the Reactor to
     * @param poolSize  					the number of WorkerThreads to include in the ThreadPool
     * @param asyncServerProtocolFactory  	the asyncServerProtocol factory to work with
     * @param tokenizerFactory 				the tokenizer factory to work with
     * @throws IOException if some I/O problems arise during connection
     */
    public Reactor(int port, int poolSize, AsyncServerProtocolFactory asyncServerProtocolFactory, TokenizerFactory tokenizerFactory) {
        this.port = port;
        this.poolSize = poolSize;
        this.asyncServerProtocolFactory = asyncServerProtocolFactory;
        this.tokenizerFactory = tokenizerFactory;
    }

    /**
     * Create a non-blocking server socket channel and bind to to the Reactor
     * port
     */
    private ServerSocketChannel createServerSocket(int port) throws IOException {
        try {
            ServerSocketChannel ssChannel = ServerSocketChannel.open();
            ssChannel.configureBlocking(false);
            ssChannel.socket().bind(new InetSocketAddress(port));
            return ssChannel;
        } catch (IOException e) {
            logger.info("Port " + port + " is busy\n");
            throw e;
        }
    }

    /**
     * Main operation of the Reactor:
     * <UL>
     * <LI>Uses the <CODE>Selector.select()</CODE> method to find new
     * requests from clients
     * <LI>For each request in the selection set:
     * <UL>
     * If it is <B>acceptable</B>, use the ConnectionAcceptor to accept it,
     * create a new ConnectionHandler for it register it to the Selector
     * <LI>If it is <B>readable</B>, use the ConnectionHandler to read it,
     * extract messages and insert them to the ThreadPool
     * </UL>
     */
    public void run() {
        // Create & start the ThreadPool
        ExecutorService executor = Executors.newFixedThreadPool(this.poolSize);
        Selector selector = null;
        ServerSocketChannel ssChannel = null;

        try {
            selector = Selector.open();
            ssChannel = createServerSocket(this.port);
        } catch (IOException e) {
            logger.info("cannot create the selector -- server socket is busy?\n");
            return;
        }

        this.reactorData = new ReactorData(executor, selector, this.asyncServerProtocolFactory, this.tokenizerFactory);
        ConnectionAcceptor connectionAcceptor = new ConnectionAcceptor(ssChannel, this.reactorData);

        // Bind the server socket channel to the selector, with the new
        // acceptor as attachment

        try {
            ssChannel.register(selector, SelectionKey.OP_ACCEPT, connectionAcceptor);
        } catch (ClosedChannelException e) {
            logger.info("server channel seems to be closed!\n");
            return;
        }
        int i=0;
        while (this.shouldRun && selector.isOpen()) {
            // Wait for an event
            try {
                selector.select();
            } catch (IOException e) {
                logger.info("trouble with selector: " + e.getMessage() + "\n");
                continue;
            }

            // Get list of selection keys with pending events
            Iterator<SelectionKey> it = selector.selectedKeys().iterator();

            // Process each key
            while (it.hasNext()) {
                // Get the selection key
                SelectionKey selKey = it.next();

                // Remove it from the list to indicate that it is being
                // processed. it.remove removes the last item returned by next.
                it.remove();

                // Check if it's a connection request
                if (selKey.isValid() && selKey.isAcceptable()) {
                    logger.info("Accepting a connection\n");
                    ConnectionAcceptor acceptor = (ConnectionAcceptor) selKey.attachment();
                    try {
                        acceptor.accept(i);
                        i++;
                    } catch (IOException e) {
                        logger.info("problem accepting a new connection: " + e.getMessage() + "\n");
                    }
                    continue;
                }
                // Check if a message has been sent
                if (selKey.isValid() && selKey.isReadable()) {
                    ConnectionHandler handler = (ConnectionHandler) selKey.attachment();
                    logger.info("Channel is ready for reading" + "  (Connction ID: " + handler.getConnectionID() + ")\n");
                    handler.read();
                }
                // Check if there are messages to send
                if (selKey.isValid() && selKey.isWritable()) {
                    ConnectionHandler handler = (ConnectionHandler) selKey.attachment();
                    logger.info("Channel is ready for writing" + "  (Connction ID: " + handler.getConnectionID() + ")\n");
                    handler.write();
                }
            }
        }
        stopReactor();
    }

    /**
     * Returns the listening port of the Reactor
     *
     * @return the listening port of the Reactor
     */
    public int getPort() {
        return this.port;
    }

    /**
     * Stops the Reactor activity, including the Reactor thread and the Worker
     * Threads in the Thread Pool.
     */
    public synchronized void stopReactor() {
        if (!this.shouldRun)
            return;
        this.shouldRun = false;
        this.reactorData.getSelector().wakeup(); // Force select() to return
        this.reactorData.getExecutorService().shutdown();
        try {
        	this.reactorData.getExecutorService().awaitTermination(2000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            // Someone didn't have patience to wait for the executor pool to
            // close
            e.printStackTrace();
        }
    }

    /**
     * Main program, used for demonstration purposes. Create and run a
     * Reactor-based server for the Echo protocol. Listening port number and
     * number of threads in the thread pool are read from the command line.
     */
    public static void main(String args[]) {
        if (args.length != 2) {
            System.err.println("Usage: java Reactor <port> <pool_size>");
            System.exit(1);
        }

        try {
            int port = Integer.parseInt(args[0]);
            int poolSize = Integer.parseInt(args[1]);

            Reactor reactor = startTBGPServer(port, poolSize);

            Thread thread = new Thread(reactor);
            thread.start();
            logger.info("Reactor is ready on port " + reactor.getPort() + "\n");
            thread.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Reactor startTBGPServer(int port, int poolSize) {
        AsyncServerProtocolFactory asyncServerProtocolFactory = new AsyncServerProtocolFactory() {
            public AsyncServerProtocol create(String connectionID) {
                return new TBGP(connectionID);
            }
        };


        final Charset charset = Charset.forName("UTF-8");
        TokenizerFactory tokenizerFactory = new TokenizerFactory() {
            public MessageTokenizer create() {
                return new FixedSeparatorMessageTokenizer("\n", charset);
            }
        };

        Reactor reactor = new Reactor(port, poolSize, asyncServerProtocolFactory, tokenizerFactory);
        return reactor;
    }
}
