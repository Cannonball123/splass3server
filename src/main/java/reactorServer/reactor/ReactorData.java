package reactorServer.reactor;

import java.util.concurrent.ExecutorService;
import java.nio.channels.Selector;

import messages.TokenizerFactory;
import protocol.AsyncServerProtocolFactory;


/**
 * A simple data structure that holds information about the reactor, including getter methods
 */
public class ReactorData {

    private final ExecutorService executorService;
    private final Selector selector;
    private final AsyncServerProtocolFactory asyncServerProtocolFactory;
    private final TokenizerFactory tokenizerFactory;
    
	public ReactorData(ExecutorService executorService, Selector selector, AsyncServerProtocolFactory asyncServerProtocolFactory, TokenizerFactory tokenizerFactory) {
		this.executorService = executorService;
		this.selector = selector;
		this.asyncServerProtocolFactory = asyncServerProtocolFactory;
		this.tokenizerFactory = tokenizerFactory;
	}

	
	public ExecutorService getExecutorService() {
		return this.executorService;
	}
	
	public Selector getSelector() {
		return this.selector;
	}
	
	public AsyncServerProtocolFactory getAsyncServerProtocolFactory() {
		return this.asyncServerProtocolFactory;
	}

	public TokenizerFactory getTokenizerFactory() {
		return tokenizerFactory;
	}

}
