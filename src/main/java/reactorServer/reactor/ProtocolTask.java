package reactorServer.reactor;

import messages.Message;
import messages.MessageTokenizer;
import protocol.ServerProtocol;
import reactorServer.protocol.TBGP;

import java.nio.ByteBuffer;


/**
 * This class supplies some data to the protocol, which then processes the data,
 * possibly returning a reply. This class is implemented as an executor task.
 * 
 */
public class ProtocolTask implements Runnable {

	private final TBGP serverProtocol;
	private final MessageTokenizer messageTokenizer;
	private final ConnectionHandler connectionHandler;

	public ProtocolTask(final ServerProtocol protocol, final MessageTokenizer tokenizer, final ConnectionHandler h) {
		this.serverProtocol = (TBGP) protocol;
		this.messageTokenizer = tokenizer;
		this.connectionHandler = h;

	}

	// we synchronize on ourselves, in case we are executed by several threads
	// from the thread pool.
	public synchronized void run() {
      // go over all complete messages and process them.
      while (this.messageTokenizer.hasMessage()) {
          Message gameMessage = this.messageTokenizer.nextMessage();
          gameMessage.setConnectionID(Integer.toString(this.connectionHandler.getConnectionID()));
		  //String s = String.valueOf(_handler.getId());
		  //String message = msg.getFullMessage();
		  //StringMessage stringMessage = new StringMessage(message);
          this.serverProtocol.setConnectionID(Integer.toString(this.connectionHandler.getConnectionID()));
          this.serverProtocol.processMessage(gameMessage, msg1 -> {
			 //StringMessage stringMessage = new StringMessage((String) msg1);
			 ByteBuffer byteBuffer = this.messageTokenizer.getBytesForMessage(msg1);
			 this.connectionHandler.addOutData(byteBuffer);
		  });

      }
	}

	public void addBytes(ByteBuffer b) {
		this.messageTokenizer.addBytes(b);
	}
}
