package reactorServer.reactor;

import java.nio.channels.SocketChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.ClosedChannelException;
import java.nio.ByteBuffer;
import java.io.IOException;
import java.net.SocketAddress;
import java.util.Vector;
import java.util.logging.Logger;

import messages.MessageTokenizer;
import protocol.AsyncServerProtocol;


/**
 * Handles messages from clients
 */
public class ConnectionHandler {

	private int connectionID;
	
	private static final int BUFFER_SIZE = 1024;

	protected final SocketChannel socketChannel;

	protected final ReactorData reactorData;

	protected final AsyncServerProtocol asyncServerProtocol;
	protected final MessageTokenizer messageTokenizer;

	protected Vector<ByteBuffer> outData = new Vector<ByteBuffer>();

	protected final SelectionKey selectionKey;

	private static final Logger logger = Logger.getLogger("edu.spl.reactor");

	private ProtocolTask protocolTask = null;
	
	
	/**
	 * Creates a new ConnectionHandler object
	 * 
	 * @param sChannel the SocketChannel of the client.
	 * @param data a reference to a ReactorData object.
	 */
	private ConnectionHandler(int connectionID, SocketChannel sChannel, ReactorData data, SelectionKey key) {
		this.connectionID = connectionID;
		this.socketChannel = sChannel;
		this.reactorData = data;
		this.asyncServerProtocol = this.reactorData.getAsyncServerProtocolFactory().create(Integer.toString(connectionID));
		this.messageTokenizer = this.reactorData.getTokenizerFactory().create();
		this.selectionKey = key;

	}

	// make sure 'this' does not escape b4 the object is fully constructed!
	private void initialize() {
		this.selectionKey.attach(this);
		this.protocolTask = new ProtocolTask(this.asyncServerProtocol, this.messageTokenizer, this);
	}

	public static ConnectionHandler create(int connectionID, SocketChannel sChannel, ReactorData data, SelectionKey key) {
		ConnectionHandler h = new ConnectionHandler(connectionID, sChannel, data, key);
		h.initialize();
		return h;
	}

	public synchronized void addOutData(ByteBuffer buf) {
		this.outData.add(buf);
		switchToReadWriteMode();
	}

	private void closeConnection() {
		// remove from the selector.
		this.selectionKey.cancel();
		try {
			this.socketChannel.close();
		} catch (IOException ignored) {
			ignored = null;
		}
	}
	
	public int getConnectionID(){
		return this.connectionID;
	}

	/**
	 * Reads incoming data from the client:
	 * <UL>
	 * <LI>Reads some bytes from the SocketChannel
	 * <LI>create a protocolTask, to process this data, possibly generating an
	 * answer
	 * <LI>Inserts the Task to the ThreadPool
	 * </UL>
	 * 
	 * @throws
	 * 
	 * @throws IOException
	 *             in case of an IOException during reading
	 */
	public void read() {
		// do not read if protocol has terminated. only write of pending data is
		// allowed
		if (this.asyncServerProtocol.shouldClose()) {
			return;
		}

		SocketAddress address = this.socketChannel.socket().getRemoteSocketAddress();
		logger.info("Reading from " + address + "  (Connction ID: " + this.connectionID + ")\n");

		ByteBuffer buf = ByteBuffer.allocate(BUFFER_SIZE);
		int numBytesRead = 0;
		try {
			numBytesRead = this.socketChannel.read(buf);
		} catch (IOException e) {
			numBytesRead = -1;
		}
		// is the channel closed??
		if (numBytesRead == -1) {
			// No more bytes can be read from the channel
			logger.info("client on " + address + " has disconnected\n");
			closeConnection();
			// tell the protocol that the connection terminated.
			this.asyncServerProtocol.connectionTerminated();
			return;
		}

		//add the buffer to the protocol task
		buf.flip();
		this.protocolTask.addBytes(buf);
		// add the protocol task to the reactor
		this.reactorData.getExecutorService().execute(this.protocolTask);
	}

	/**
	 * attempts to send data to the client<br/>
	 * if all the data has been successfully sent, the ConnectionHandler will
	 * automatically switch to read only mode, otherwise it'll stay in it's
	 * current mode (which is read / write).
	 * 
	 * @throws IOException
	 *             if the write operation fails
	 * @throws ClosedChannelException
	 *             if the channel have been closed while registering to the Selector
	 */
	public synchronized void write() {
		if (this.outData.size() == 0) {
			// if nothing left in the output string, go back to read mode
			switchToReadOnlyMode();
			return;
		}
		// if there is something to send
		ByteBuffer buf = this.outData.remove(0);
		if (buf.remaining() != 0) {
			try {
				this.socketChannel.write(buf);
			} catch (IOException e) {
				// this should never happen.
				e.printStackTrace();
			}
			// check if the buffer contains more data
			if (buf.remaining() != 0) {
				this.outData.add(0, buf);
			}
		}
		// check if the protocol indicated close.
		if (this.asyncServerProtocol.shouldClose()) {
			switchToWriteOnlyMode();
			if (buf.remaining() == 0) {
				closeConnection();
				SocketAddress address = this.socketChannel.socket().getRemoteSocketAddress();
				logger.info("disconnecting client on " + address + "\n");
			}
		}
	}

	/**
	 * switches the handler to read / write TODO Auto-generated catch blockmode
	 * 
	 * @throws ClosedChannelException
	 *             if the channel is closed
	 */
	public void switchToReadWriteMode() {
		this.selectionKey.interestOps(SelectionKey.OP_READ | SelectionKey.OP_WRITE);
		this.reactorData.getSelector().wakeup();
	}

	/**
	 * switches the handler to read only mode
	 * 
	 * @throws ClosedChannelException
	 *             if the channel is closed
	 */
	public void switchToReadOnlyMode() {
		this.selectionKey.interestOps(SelectionKey.OP_READ);
		this.reactorData.getSelector().wakeup();
	}

	/**
	 * switches the handler to write only mode
	 * 
	 * @throws ClosedChannelException
	 *             if the channel is closed
	 */
	public void switchToWriteOnlyMode() {
		this.selectionKey.interestOps(SelectionKey.OP_WRITE);
		this.reactorData.getSelector().wakeup();
	}

}
