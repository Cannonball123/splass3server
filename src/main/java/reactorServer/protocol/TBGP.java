package reactorServer.protocol;

import java.io.IOException;

import gameLogic.GameManager;

import messages.Message;

import protocol.AsyncServerProtocol;
import protocol.ProtocolCallback;

public class TBGP implements AsyncServerProtocol {
	
	private String connectionID;
	private boolean shouldClose;
	
	public TBGP(String connectionID) {
		this.connectionID = connectionID;
		this.shouldClose = false;
	}
	

	public String getConnectionID() {
		return connectionID;
	}

	public void setConnectionID(String connectionID) {
		this.connectionID = connectionID;
	}

	@Override
	public void processMessage(Message msg, ProtocolCallback callback) {

		try {
			GameManager.getInstance().processMessage(msg, callback);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public boolean isEnd(Message msg) {
		return msg.getMessageCommand().equals("QUIT");
	}

	@Override
	public boolean shouldClose() {
		return shouldClose;
	}

	@Override
	public void connectionTerminated() {
		this.shouldClose = true;
	}

}
