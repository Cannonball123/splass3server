package messages;

import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CharacterCodingException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.util.Vector;

public class FixedSeparatorMessageTokenizer implements MessageTokenizer {
	

	private final String messageSeparator;

	private final StringBuffer stringBuffer = new StringBuffer();
   
	/**
	 * the fifo queue, which holds data coming from the socket. Access to the
	 * queue is serialized, to ensure correct processing order.
	 */
	private final Vector<ByteBuffer> _buffers = new Vector<ByteBuffer>();

	private final CharsetDecoder charsetDecoder;
	private final CharsetEncoder charsetEncoder;

	public FixedSeparatorMessageTokenizer(String separator, Charset charset) {
		this.messageSeparator = separator;
		this.charsetDecoder = charset.newDecoder();
		this.charsetEncoder = charset.newEncoder();
	}

	/**
	 * Add some bytes to the message.  
	 * Bytes are converted to chars, and appended to the internal StringBuffer.
	 * Complete messages can be retrieved using the nextMessage() method.
	 *
	 * @param bytes an array of bytes to be appended to the message.
	 */
	public synchronized void addBytes(ByteBuffer bytes) {
		_buffers.add(bytes);
	}

	/**
	 * Is there a complete message ready?.
	 * @return true the next call to nextMessage() will not return null, false otherwise.
	 */
	public synchronized boolean hasMessage() {
		while(_buffers.size() > 0) {
			ByteBuffer bytes = _buffers.remove(0);
			CharBuffer chars = CharBuffer.allocate(bytes.remaining());
			this.charsetDecoder.decode(bytes, chars, false); // false: more bytes may follow. Any unused bytes are kept in the decoder.
			chars.flip();
			this.stringBuffer.append(chars);
		}
		return this.stringBuffer.indexOf(this.messageSeparator) > -1;
	}

	/**
	 * Get the next complete message if it exists, advancing the tokenizer to the next message.
	 * @return the next complete message, and null if no complete message exist.
	 */
	public synchronized StringMessage nextMessage() {
		String message = null;
		int messageEnd = this.stringBuffer.indexOf(this.messageSeparator);
		if (messageEnd > -1) {
			message = this.stringBuffer.substring(0, messageEnd);
			this.stringBuffer.delete(0, messageEnd+this.messageSeparator.length());
		}
		return new StringMessage(message);
	}

	/**
	 * Convert the String message into bytes representation, taking care of encoding and framing.
	 *
	 * @return a ByteBuffer with the message content converted to bytes, after framing information has been added.
	 */
	public ByteBuffer getBytesForMessage(Message msg)  throws CharacterCodingException {
		StringBuilder sb = new StringBuilder(msg.getFullMessage());
		sb.append(this.messageSeparator);
		ByteBuffer bb = this.charsetEncoder.encode(CharBuffer.wrap(sb));
		return bb;
	}

}
