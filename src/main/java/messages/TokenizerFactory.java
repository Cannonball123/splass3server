package messages;

/**
 * A tokenizer factory.
 * 
 * @author Avi
 */
public interface TokenizerFactory {
	
	MessageTokenizer create();
	
}
