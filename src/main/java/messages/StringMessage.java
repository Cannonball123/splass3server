package messages;

public class StringMessage implements Message {
	
	private String connectionID;
	
    private String messageCommand;
    private String messageParameter;
	
    public StringMessage(String rawStringMessage) {    	
    	
    	this.connectionID = "";
		
    	String[] messageParts = rawStringMessage.split(" ", 2);
		if(messageParts.length == 1) {
			this.messageCommand = messageParts[0];
		} else if (messageParts.length == 2){
			this.messageCommand = messageParts[0];
			this.messageParameter = messageParts[1];
		}
	}

	@Override
	public String getConnectionID() {
		return this.connectionID;
	}

	@Override
	public void setConnectionID(String messageID) {
		this.connectionID = messageID;
	}

	@Override
	public String getMessageCommand() {
		return this.messageCommand;
	}

	@Override
	public void setMessageCommand(String messageCommand) {
		this.messageCommand = messageCommand;
	}

	@Override
	public String getMessageParameter() {
		return this.messageParameter;
	}

	@Override
	public void setMessageParameter(String messageParameter) {
		this.messageParameter = messageParameter;
	}
	
	@Override
	public String getFullMessage() {
		return this.messageCommand + " " + this.messageParameter;
	}
	
	@Override
	public boolean equals(Object other) {
		return (this.messageCommand + " " + this.messageParameter).equals(other);
	}

}
