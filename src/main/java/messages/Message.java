package messages;

/**
 * A message of type MessageType.
 * 
 * @author Avi
 */
public interface Message {
	
	public String getConnectionID();
	
	public void setConnectionID(String messageID);
	
	public String getMessageCommand();
	
	public void setMessageCommand(String messageCommand);
	
	public String getMessageParameter();
	
	public void setMessageParameter(String messageParameter);
	
	public String getFullMessage();

}
