package gameLogic;


import protocol.ProtocolCallback;

public class Player {
	
	private String connectionID;
	private String name;
    private ProtocolCallback callback;
    public String roundMsg;
    public int singleRoundPoints;		// TODO: no public variables!~
    
    public Player (String connectionID, String name, ProtocolCallback clientCallback){
    	this.connectionID = connectionID;
    	this.name = name;
    	this.callback = clientCallback;
    	this.roundMsg = "";
        this.singleRoundPoints = 0;
    }

    public String getName(){
        return name;
    }
    public String getId() {
        return connectionID;
    }

    public ProtocolCallback getCallback() {
        return callback;
    }
}