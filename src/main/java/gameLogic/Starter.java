package gameLogic;

import reactorServer.reactor.Reactor;
import tpcServer.server.MultipleClientProtocolServer;

import java.io.IOException;
import java.util.Scanner;

/**
 * Created by kobi on 1/31/16.
 */


public class Starter {
	
	// args are port number and pool size
    public static void main(String []args){
        
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Please pick a server. 1 for tpc 2 for reactor");
        String t = scanner.nextLine();
        while(!t.equals("1") && !t.equals("2")) {
        	System.out.println("Invalid input, Try again.\n1 for tpc 2 for reactor");
        	t = scanner.nextLine();
        }
        scanner.close();
        
        if (t.equals("1")){
            try {
                MultipleClientProtocolServer.main(args);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (t.equals("2")){
            Reactor.main(args);
        }

    }
}
