package gameLogic;

import protocol.AsyncServerProtocol;
import protocol.AsyncServerProtocolFactory;
import reactorServer.protocol.TBGP;

public class AsyncBlufferProtocolFactory implements AsyncServerProtocolFactory {

	@Override
	public AsyncServerProtocol create(String id) {
		return new TBGP(id);
	}

}
