package gameLogic;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by kobi on 1/14/16.
 */
public class parser {
    public static void main(String []args){
        parse(new ArrayList<>());
    }
    public static void parse(ArrayList<Question> tofill){
        Gson gson = new Gson();
        System.out.println("please input file name with .txt");
        Reader reader = null;
        String file = "jason.Json";
        Scanner sc = new Scanner(System.in);

        boolean readSuccess=false;
        while(!readSuccess){
            try {
                reader = new FileReader(file);
                readSuccess = true;
            } catch (FileNotFoundException e) {
                System.out.println("File not found - please enter new file name:");
                file = sc.nextLine();
            }
        }

        sc.close();

        Wrapper p = gson.fromJson(reader, Wrapper.class);
        p.questions.forEach(q->tofill.add(new Question(q.realans,q.question)));

    }

    class Wrapper{
        @SerializedName("questions")
        ArrayList <q> questions= new ArrayList<>();
    }
    class q{
        @SerializedName("questionText")
        String question;
        @SerializedName("realAnswer")
        String realans;

    }
}
