package gameLogic;
import java.io.IOException;
import java.util.ArrayList;

import messages.Message;
import messages.StringMessage;
import protocol.ProtocolCallback;


public class GameRoom {
    public  ArrayList<Player> _players=new ArrayList<>();
    public boolean started =false;
    private String Name;
    private BlufferGame bluffer;



    public GameRoom(String name) {
        this.Name=name;
    }

    public void joinRoom(Player player) {

        if (!started) {
            _players.add(player);
            System.out.println(player.getName()+" is joining "+this.Name);

            try {
                player.getCallback().sendMessage(new StringMessage("SYSMSG JOIN ACCEPTED"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else try {
            player.getCallback().sendMessage(new StringMessage("SYSMSG JOIN REJECTED"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getName() {
        return this.Name;
    }

    public void sendToAll(String msg) {

        _players.forEach(player -> {
            try {
                player.getCallback().sendMessage(new StringMessage(msg));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void startGame(ProtocolCallback callback, String game) {
        if (game.equals("BLUFFER")){
       this.started = true;
        try {
            callback.sendMessage(new StringMessage("SYSMSG STARTGAME ACCEPTED"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        sendToAll("SYSMSG BLUFFERGAME STARTED");
        this.bluffer = new BlufferGame(this);
        bluffer.startGame(callback);
    }
    else {
            try {
                callback.sendMessage(new StringMessage("SYSMSG STARTGAME REJECTED"));
            } catch (IOException e) {
                e.printStackTrace();
            }

        }}

    public void leaveRoom(Message gameMessage, ProtocolCallback callback) {
        System.out.println("quit requested");
        if (started) {
            try {
                callback.sendMessage(new StringMessage("SYSMSG REQUEST REJECTED"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                callback.sendMessage(new StringMessage("SYSMSG ROOM LEFT"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            String id = gameMessage.getConnectionID();
            
            Player t = null;
            for(Player player:_players){
            	if(player.getId().equals(id)) t = player; 
            }
            
            _players.remove(t);
            
        }
    }


    public void SendPlayer(Player player, String message) {
        try {
            player.getCallback().sendMessage(new StringMessage(message));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void SendAll(String message, String senderId) {
        if(senderId==null) {
            _players.forEach(player1 -> System.out.println(player1.getName()));
            _players.forEach(player -> {
                try {
                    player.getCallback().sendMessage(new StringMessage(message));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }else{
            _players.forEach(player1 -> {
                if(!player1.getId().equals(senderId)) {
                    System.out.println(player1.getName());
                }
            });
            _players.forEach(player -> {
                if(!player.getId().equals(senderId)) {
                    try {
                        player.getCallback().sendMessage(new StringMessage(message));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }


    public void txtresponse(Message gameMessage)  {
        this.bluffer.txtresponse(gameMessage);
    }

    public void selectResp(Message gameMessage, ProtocolCallback callback) {
        this.bluffer.selectResp(gameMessage, callback);
    }
}