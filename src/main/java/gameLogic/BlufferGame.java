package gameLogic;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import messages.Message;
import messages.StringMessage;
import protocol.ProtocolCallback;

/**
 * Created by avi on 15/01/16.
 */
public class BlufferGame {
    private ArrayList<Question> _question = new ArrayList<>();
    private ArrayList<String> shufelled=new ArrayList<>();
    private HashMap<Player, String> _playerAns = new HashMap<>();
    private HashMap<Player, Integer> _playersPoints = new HashMap<>();
    GameRoom room;
    int currentQuestion = 0;


    public BlufferGame(GameRoom room){
        this.room = room;
        for(Player p: room._players){
            _playersPoints.put(p, 0);
        }
    }



    public void startGame(ProtocolCallback callback) {
    	_question = getQuestions();
        room.sendToAll("ASKTXT " + _question.get(currentQuestion).getQuestion());
    }
    
    
    public ArrayList<Question> getQuestions() {

        ArrayList<Question> q=new ArrayList<Question>();
        parser.parse(q);
        ArrayList<Question> toReuturn=new ArrayList<>();
        for (int i=0; i<=2; i++){
            toReuturn.add(q.get(i));
        }
        return toReuturn;
    }


    public void txtresponse(Message gameMessage)  {
        //finding the player who gave the message
        Player p = null;
        for (Player player : room._players) if (player.getId() .equals(gameMessage.getConnectionID()))  {
            p = player;
        }
        //submitting the bluff answer
        String ans = gameMessage.getMessageParameter();
        ans = ans.toLowerCase();
        _question.get(currentQuestion).addPlayerAnswer(ans, p);

        try {
            p.getCallback().sendMessage(new StringMessage("SYSMSG TXTRESP ACCEPTED"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        //if it was the last answer - sending the other players the answers to choose from.

        if (_question.get(currentQuestion).getSubmittedAnswersNumber()==room._players.size())
        {
            shufelled = _question.get(currentQuestion).getAllAnswers();
            String s="";
            int i=1;
            for (String k: shufelled ) {
                s+=i+"."+k+" ";
                i++;
            }
            room.SendAll("ASKCHOICES " +s, null);
        }

    }


    public void selectResp(Message gameMessage, ProtocolCallback callback) {
        //adding the right answer
        room._players.forEach(p -> {
            if (p.getId().equals(gameMessage.getConnectionID())) {
                _playerAns.put(p, gameMessage.getMessageParameter());
                try {
                    p.getCallback().sendMessage(new StringMessage("SYSMSG SELECTRESP ACCEPTED"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        //if the players finished answering for this turn..
        if (_playerAns.size() == room._players.size()) {
            room.SendAll("GAMEMSG The correct answer is: " + this._question.get(this.currentQuestion).getRealAnswer(), null);
            _playerAns.forEach((player, ans) -> {
                int i =Integer.parseInt(ans)-1;
                if(shufelled.get(i).equals(_question.get(currentQuestion).getRealAnswer())) {
                    player.roundMsg += "correct! ";
                    player.singleRoundPoints += 10;
                    _playersPoints.replace(player, _playersPoints.get(player)+10);
                } else {
                    System.out.println("Wrong ansewr");
                    player.roundMsg += "wrong! ";
                    Player P=_question.get(currentQuestion).getAnswerSender(shufelled.get(i));
                    _playersPoints.replace(P, _playersPoints.get(P)+5);
                    P.singleRoundPoints +=5;
                }
            });
            //sending the players a turn summary
            String summary = "GAMEMSG ";
            for (Player P : room._players) {
                summary += P.roundMsg + " +" + P.singleRoundPoints +"pts";
                try {
                    P.getCallback().sendMessage(new StringMessage(summary));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                P.roundMsg = "";
                P.singleRoundPoints = 0;
                summary = "GAMEMSG ";
            }
            //room.SendAll(summary, null);
            //deleting the answers of the current turn
            _playerAns.clear();
            //if the game is over, finding the winner and declaring it.
            if (currentQuestion == 2) {
                summary += "Summary: ";
                //room.sendToAll("GAMEOVER");
                //Player winner = null;
                //int max = 0;
                for (Player player1 : room._players) {
                    summary += player1.getName() + ": " + _playersPoints.get(player1) + "pts,";
                    /*
                    if (_playersPoints.get(player1) > max) {
                        max = _playersPoints.get(player1);
                        //winner = player1;
                    }
                    */
                }
                summary = summary.substring(0, summary.length());
                room.started=false;
                room.SendAll(summary, null);
            } else {
                currentQuestion++;
                room.SendAll(_question.get(currentQuestion).getQuestion(), null);
            }
        }
    }



}
