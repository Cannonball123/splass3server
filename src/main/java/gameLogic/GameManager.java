package gameLogic;


import protocol.ProtocolCallback;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import messages.Message;
import messages.StringMessage;

public class GameManager {
    private GameManager() {
        _games.add("BLUFFER");
    }

    private static GameManager ourInstance = new GameManager();
    public static GameManager getInstance(){return ourInstance;}
    private HashMap<String, GameRoom> _roomsById= new HashMap<>();
    ArrayList<GameRoom> _rooms = new ArrayList<>();
    ArrayList<Player> _players = new ArrayList<>();
    HashMap <ProtocolCallback, GameRoom> _roomsByCallback= new HashMap<>();
    ArrayList<String>_games = new ArrayList<>();
    public void processMessage(Message gameMessage, ProtocolCallback callback) throws IOException {
        switch (gameMessage.getMessageCommand()){
            case ("NICK"):
                boolean USED = false;
                for (Player player:_players){
                    if (player.getName().equals(gameMessage.getMessageParameter())){
                        USED = true;
                    }
                }
                if (USED){
                	callback.sendMessage(new StringMessage("SYSMSG NICK REJECTED"));
                }
                if (!USED){
                    Player player = new Player(gameMessage.getConnectionID(), gameMessage.getMessageParameter(), callback);
                    _players.add(player);
                    callback.sendMessage(new StringMessage("SYSMSG NICK ACCEPTED"));
                }
                break;
            case ("JOIN"):
                if (_roomsById.containsKey(gameMessage.getConnectionID())){
                    callback.sendMessage(new StringMessage("SYSMSG JOIN REJECTED"));
                }
                else{
                boolean Exist = false;
                for (GameRoom gameRoom:_rooms){
                    if (gameRoom.getName().equals(gameMessage.getMessageParameter())){
                        Exist = true;
                        for (Player player: _players)
                        {
                            if (player.getId().equals(gameMessage.getConnectionID())){
                                System.out.println("joining a room...");
                                gameRoom.joinRoom(player);
                                _roomsById.put(gameMessage.getConnectionID(), gameRoom);
                                break;
                            }
                        }
                    }
                }

                if (!Exist){
                    GameRoom gameRoom=new GameRoom(gameMessage.getMessageParameter());
                    for (Player player: _players)
                    {
                        if (player.getId() .equals(gameMessage.getConnectionID()) ){
                            gameRoom.joinRoom(player);
                            _rooms.add(gameRoom);
                            _roomsById.put(gameMessage.getConnectionID(), gameRoom);
                            _roomsByCallback.put(callback, gameRoom);
                            break;
                        }
                    }
                }
                }
                break;
            case ("MSG"):
                String senderName = "";
                for(Player p : _players){
                    if(p.getId().equals(gameMessage.getConnectionID())){
                        senderName = p.getName();
                    }
                }
                _roomsById.get(gameMessage.getConnectionID()).SendAll("USRMSG " + senderName + ": " + gameMessage.getMessageParameter(), gameMessage.getConnectionID());
                callback.sendMessage(new StringMessage("SYSMSG MSG ACCEPTED"));
                break;
            case ("LISTGAMES"):

                    String gms = "SYSMSG LISTGAMES ACCEPTED ";
                    System.out.println(gms);
                    for(String gm: _games){

                        gms +=gm + " ";
                    }

                    callback.sendMessage(new StringMessage(gms));
                break;
            case ("STARTGAME"):
                    _roomsById.get(gameMessage.getConnectionID()).startGame(callback, gameMessage.getMessageParameter());
                break;
            case ("TXTRESP"):

                    _roomsById.get(gameMessage.getConnectionID()).txtresponse(gameMessage);
                break;
            case ("QUIT"):
                    GameRoom room = _roomsById.get(gameMessage.getConnectionID());
                    if(room!=null) {
                        _roomsById.remove(gameMessage.getConnectionID()).leaveRoom(gameMessage, callback);
                    }else{
                        System.out.println("TERMINATED");
                        callback.sendMessage(new StringMessage("SYSMSG QUIT ACCEPTED"));
                    }
                break;

            case ("SELECTRESP"):
                    _roomsById.get(gameMessage.getConnectionID()).selectResp(gameMessage, callback);
                break;



    }


}
}
