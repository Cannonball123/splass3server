package gameLogic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class Question {
    
	private String question;
	private String realAnswer;
	private HashMap<String, Player> playerAnswers = new HashMap<>(); // TODO: needs refactoring. what if 2 players send the same answer?
    
    
    public Question(String realAnswer, String question){
        this.realAnswer = realAnswer;
        this.question = question;
    }
    
    
    public String getQuestion() {
        return question;
    }
    
    public String getRealAnswer(){
    	return realAnswer;
    }
    
    
    public void addPlayerAnswer(String answer, Player player){
        playerAnswers.putIfAbsent(answer, player);
    }
    
    public Player getAnswerSender(String s){
        return playerAnswers.get(s);
    }
    
    public ArrayList<String> getAllAnswers(){
    	ArrayList<String> _ansers = new ArrayList<>();
        _ansers.add(realAnswer);
        playerAnswers.keySet().forEach(a -> {
            _ansers.add(a);
        });
        Collections.shuffle(_ansers);

        return _ansers;
    }

    public int getSubmittedAnswersNumber() {
        return playerAnswers.size();
    }
}
