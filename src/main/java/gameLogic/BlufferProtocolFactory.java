package gameLogic;

import tpcServer.protocol.TBGP;
import protocol.ServerProtocol;
import protocol.ServerProtocolFactory;

public class BlufferProtocolFactory implements ServerProtocolFactory {
	
	@Override
	public ServerProtocol create(String id){
		return new TBGP(id);
	}
}
