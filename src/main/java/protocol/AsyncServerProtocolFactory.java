package protocol;

/**
 * A factory that creates and returns a ServerProtocol.
 * 
 * @author Avi
 */
public interface AsyncServerProtocolFactory {
	
	AsyncServerProtocol create(String id);
	
}
