package protocol;

import messages.Message;

/**
 * A protocol that describes the behavior of the server.
 * 
 * Created by kobi on 1/15/16.
 */
public interface AsyncServerProtocol extends ServerProtocol {
	
    /**
    * Processes a message.
    *
    * @param msg the message to process.
    * @param callback an instance of ProtocolCallback unique to the
    *		 connection from which msg originated.
    */
    void processMessage(Message msg, ProtocolCallback callback);
    
    /**
    * Determine whether the given message is the termination message.
    *
    * @param msg the message to examine.
    * @return true if the message is the termination message, false otherwise.
    */
    boolean isEnd(Message msg);
    
    /**
    * Is the protocol in a closing state?
    * When a protocol is in a closing state , it's handler should write
    * out all pending data, and close the connection.
    *
    * @return true if the protocol is in closing state.
    */
    boolean shouldClose();
    
    /**
    * Indicate to the protocol that the client disconnected.
    */
    void connectionTerminated();
}