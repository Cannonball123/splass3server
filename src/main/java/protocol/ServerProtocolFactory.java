package protocol;

/**
 * A factory that creates and returns a ServerProtocol.
 * 
 * @author Avi
 * 
 */
public interface ServerProtocolFactory {
	
	ServerProtocol create(String id);
	
}
