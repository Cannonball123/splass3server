package protocol;

import messages.Message;

/**
 * A protocol that describes the behavior of the server.
 * 
 */
public interface ServerProtocol {
	
	/**
	 * Process a message.
	 * 
	 * @param msg the message to process.
	 * @param callback an instance of ProtocolCallback unique to the
	 * 		  connection from which msg originated.
	 */
	void processMessage(Message msg, ProtocolCallback callback);
	
	/**
	 * Determine whether the given message is the termination message.
	 * 
	 * @param msg the message to examine.
	 * @return true if the message is the termination message. false otherwise.
	 */
	boolean isEnd(Message msg);
}
