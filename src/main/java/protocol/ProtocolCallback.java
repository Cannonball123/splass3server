package protocol;

import messages.Message;

/**
 * An interface that bridges between the protocol and the server.
 * The server should implement this interface, and pass in instance of it
 * along with any message.
 * The instance passed should be unique to each TCP connection.
 * It therefore allows the protocol to identify the sending user between
 * messages, and reply at any point in time.
 *
 * @author Avi
 */
public interface ProtocolCallback {
	
	/**
	 * 
	 * @param msg message to be sent.
	 * 
	 * @throws java.io.IOException if the message could not be sent, or if the
	 * 		   connection to this client has been closed.
	 */
    void sendMessage(Message msg) throws java.io.IOException;
}
