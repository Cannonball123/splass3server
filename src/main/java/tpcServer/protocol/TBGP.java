package tpcServer.protocol;

import java.io.IOException;

import gameLogic.GameManager;

import messages.Message;

import protocol.ProtocolCallback;
import protocol.ServerProtocol;

public class TBGP implements ServerProtocol {
	
	private String connectionID;
	
	public TBGP(String connectionID) {
		this.connectionID = connectionID;
	}
	

	public String getConnectionID() {
		return connectionID;
	}

	public void setConnectionID(String connectionID) {
		this.connectionID = connectionID;
	}

	@Override
	public void processMessage(Message msg, ProtocolCallback callback) {
		// TODO: re implement this method parallel to the gameLogic package
//		String message = msg.toString();
//		String[] parts = message.split(" ", 2);
//		GameMessage gameMessage;
//		try {
//			gameMessage = new GameMessage(parts[0], parts[1], id);
//		} catch (ArrayIndexOutOfBoundsException e){
//			gameMessage = new GameMessage(parts[0], null, id);
//		}
		
		try {
			GameManager.getInstance().processMessage(msg, callback);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public boolean isEnd(Message msg) {
		return msg.getMessageCommand().equals("QUIT");
	}

}
