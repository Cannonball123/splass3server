package tpcServer.server;

import java.io.*;
import java.net.Socket;


import messages.Message;
import messages.StringMessage;

import protocol.ServerProtocol;
import tpcServer.protocol.TBGP;


public class ConnectionHandler implements Runnable {
	
	private int connectionID;
	private Socket socket;
	private TBGP serverProtocol;
	
	private BufferedReader in;
	private PrintWriter out;

	public ConnectionHandler(int connectionID, Socket socket, ServerProtocol serverProtocol)
	{
		this.connectionID = connectionID;
		this.socket = socket;
		this.serverProtocol = (TBGP) serverProtocol;
		
		in = null;
		out = null;
		
		System.out.println("Accepted connection from client!");
		System.out.println("The client is from: " + this.socket.getInetAddress() + ":" + this.socket.getPort());

	}

	public void run() {
		try {
			initialize();
		} catch (IOException e) {
			System.out.println("Error in initializing I/O");
		}
		
		try {
			process();
		} catch (IOException e) {
			System.out.println("Error in I/O");
		}
		
		System.out.println("Connection closed - bye bye...");
		close();
	}

	public void process() throws IOException {
		String rawMessage;
		Message gameMessage = null;
		
		this.serverProtocol.setConnectionID(Integer.toString(this.connectionID)); // might not be needed
		
		while (((rawMessage = in.readLine()) != null)&&(rawMessage !="")) {
			
			gameMessage = new StringMessage(rawMessage);
			gameMessage.setConnectionID(Integer.toString(this.connectionID));
			
			
			this.serverProtocol.processMessage(gameMessage, msg1 ->
				out.println(msg1.getFullMessage())
			);
			
		}
	}

	// Starts listening
	public void initialize() throws IOException
	{
		// Initialize I/O
		in = new BufferedReader(new InputStreamReader(socket.getInputStream(),"UTF-8"));
		out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(),"UTF-8"), true);
		System.out.println("I/O initialized");
	}

	// Closes the connection
	public void close() {
		try {
			if (in != null) {
				in.close();
			}
			if (out != null) {
				out.close();
			}
			socket.close();
		} catch (IOException e) {
			System.out.println("Exception in closing I/O");
			e.printStackTrace();
		}
	}

}
