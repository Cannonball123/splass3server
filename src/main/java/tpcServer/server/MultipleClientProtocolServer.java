package tpcServer.server;

import java.io.*;
import java.net.*;

import gameLogic.BlufferProtocolFactory;

import protocol.ServerProtocolFactory;


public class MultipleClientProtocolServer implements Runnable {
	private ServerSocket serverSocket;
	private int listenPort;
	private ServerProtocolFactory serverProtocolFactory;
	
	
	// Class constructor
	public MultipleClientProtocolServer(int listenPort, ServerProtocolFactory serverProtocolFactory) {
		this.serverSocket = null;
		this.listenPort = listenPort;
		this.serverProtocolFactory = serverProtocolFactory;
	}
	
	public void run() {
		// Tries to open a socket and to listen to it
		try {
			serverSocket = new ServerSocket(listenPort);
			System.out.println("Listening...");
		} catch (IOException e) {
			System.out.println("Cannot listen on port " + listenPort);
		}
		
		// Event loop. Listens for new connections. When a new connection arrives, a new thread is created
		// and started for it.
		int id = 0;
		while (true) {
			try {
				ConnectionHandler connectionHandler = new ConnectionHandler(id, serverSocket.accept(), this.serverProtocolFactory.create(Integer.toString(id)));
				System.out.println("New connection accepted on port " + listenPort + ", Connection id " + id);
				id++;
				new Thread(connectionHandler).start();
			} catch (IOException e)	{
				System.out.println("Failed to accept on port " + listenPort);
				e.printStackTrace();
			}
		}
	}
	

	// Closes the connection
	public void close() throws IOException
	{
		serverSocket.close();
	}
	
	
	// TODO: Why the hell is this a static function? can't it be a normal class?
	public static void main(String[] args) throws IOException
	{
		// Gets the port
		int port = Integer.parseInt(args[0]);
		
		// Creates a new Thread Per Client (TPC) server on a new thread and starts it
		MultipleClientProtocolServer server = new MultipleClientProtocolServer(port, new BlufferProtocolFactory());
		Thread serverThread = new Thread(server);
		serverThread.start();
		
		// Stops execution of this function and wait for the TPC server execution to end 
		try {
			serverThread.join();
		} catch (InterruptedException e) {
			System.out.println("Server stopped");
		}
	}
}
